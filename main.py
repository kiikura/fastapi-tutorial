from enum import Enum
from typing import Dict, List, Optional

from fastapi import FastAPI, Query
from pydantic import BaseModel, HttpUrl


class Image(BaseModel):
    url: HttpUrl
    name: str


class Item(BaseModel):
    name: str
    description: Optional[str] = None
    price: float
    tax: Optional[float] = None
    images: Optional[List[Image]] = None


class Offer(BaseModel):
    name: str
    description: Optional[str] = None
    price: float
    items: List[Item]


app = FastAPI()


@app.get('/')
async def root():
    return {'message': 'Hello World'}


fake_items_db = [{"item_name": "Foo"}, {
    "item_name": "Bar"}, {"item_name": "Baz"}]


@app.get("/items/")
async def list_item(skip: int = 0, limit: int = 10,
                    q: Optional[str] = Query(
                        None,
                        alias="item-query",
                        title="Query sting",
                        description="Query string for the items to search in the database that have a good match",
                        min_length=3,
                        max_length=50,
                        regex="^fixedquery$",
                        deprecated=True,
                    )):
    return fake_items_db[skip: skip + limit]


@app.get('/items/{item_id}')
async def read_item(item_id: str, q: Optional[str] = None, short: bool = False):
    item = {"item_id": item_id}
    if q:
        item.update({"q": q})
    if not short:
        item.update(
            {"description": "This is an amazing item that has a long description"}
        )
    return item


@app.post("/items/")
async def create_item(item: Item):
    item_dict = item.dict()
    if item.tax:
        price_with_tax = item.price + item.tax
        item_dict.update({"price_with_tax": price_with_tax})
    return item_dict


@app.put("/items/{item_id}")
async def update_item(item_id: int, item: Item, q: Optional[str] = None):
    result = {"item_id": item_id, **item.dict()}
    if q:
        result.update({"q": q})
    return result


@app.get('/users/me')
async def read_user_me():
    return {'user_id': 'current usesr'}


@app.get('/users/{user_id}')
def read_user(user_id: str):
    return {'user_id': user_id}


class ModelName(str, Enum):
    alexn = 'alexnet'
    resnet = 'resnet'
    lenet = 'renet'


@app.get('/models/{model_name}')
async def get_model(model_name: ModelName):
    if model_name == ModelName.alexnet:
        return {"model_name": model_name, "message": "Deep Learning FTW!"}
    if model_name.value == "lenet":
        return {"model_name": model_name, "message": "LeCNN all the images"}
    return {"model_name": model_name, "message": "Have some residuals"}


@app.get('/files/{file_path:path}')
async def get_file(file_path: str):
    return file_path


@app.post("/offers/")
async def create_offer(offer: Offer):
    return offer


@app.post("/images/multiple/")
async def create_multiple_images(images: List[Image]):
    return images


@app.post("/index-weights/")
async def create_index_weights(weights: Dict[int, float]):
    return weights
