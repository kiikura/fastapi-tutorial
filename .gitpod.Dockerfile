FROM gitpod/workspace-mysql



# Awesome Custom Build

## install neovim
RUN sudo apt-get install software-properties-common
RUN sudo add-apt-repository ppa:neovim-ppa/unstable
RUN sudo apt-get update
RUN sudo apt-get install neovim

USER gitpod
